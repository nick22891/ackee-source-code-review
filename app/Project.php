<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'start_date', 'end_date', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    protected $hidden = ['password', 'remember_token'];

    public function products () {

        return $this->hasMany('App\Product');

    }

}

