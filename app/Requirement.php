<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model {

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'requirements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'summary'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    protected $hidden = ['password', 'remember_token'];

    public function feature()
    {
        return $this->belongsTo('App\Feature');
    }

}
