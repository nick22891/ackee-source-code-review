<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Release extends Model {

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'releases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'release_date', 'owner', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    protected $hidden = ['password', 'remember_token'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function features () {

        return $this->hasMany('App\Feature');

    }

}
