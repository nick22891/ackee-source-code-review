<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/project/new', function () {

    return view('createproject');

});

Route::post('/project/create', 'ProjectController@createProject');

Route::get('/projects', 'ProjectController@allProjects');

Route::get('/project/{id}', 'ProjectController@getProjectById');

Route::get('/project/{id}/products', 'ProjectController@getProductsByProject');

Route::get('/products', 'ProjectController@allProducts');

Route::get('/product/{id}', 'ProjectController@getProductById');

Route::get('/product/{id}/releases', 'ProjectController@getReleasesByProduct');

Route::get('/releases', 'ProjectController@allReleases');

Route::get('/release/{id}', 'ProjectController@getReleaseById');


