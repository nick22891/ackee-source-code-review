<?php namespace App\Http\Controllers;

use App\Project;
use App\Product;
use App\Release;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller;

class ProjectController extends Controller {

    function createProject () {

        //Project::create(['name' => 'TestName', 'start_date' => '01-01-2016', 'end_date' => '31-12-2016', 'status' => 'Started']);

        $project = new Project();

        $project->name = Input::get('name');

        $project->start_date = Input::get('start_date');

        $project->end_date = Input::get('end_date');

        $project->status = "Draft";

        try {

            $project->save();

        }

        catch (\Exception $e){

            return $e->getMessage();

        }

        return "Project Created!";

    }

    function allProjects () {

        return Project::all();

    }

    function getProjectById ($id) {

        return Project::find($id);

    }

    function allProducts () {

        return Product::all();

    }

    function getProductById ($id) {

        return Product::find($id);

    }

    function allReleases () {

        return Release::all();

    }

    function getReleaseById ($id) {

        return Release::find($id);

    }

    function allFeatures () {

        return Feature::all();

    }

    function getFeatureById ($id) {

        return Feature::find($id);

    }

    function allRequirements () {

        return Requirement::all();

    }

    function getRequirementById ($id) {

        return Requirement::find($id);

    }

    function getProductsByProject ($project_id) {

        $products = Project::find($project_id)->products;

        return $products;

    }

    function getReleasesByProduct ($product_id) {

        $releases = Product::find($product_id)->releases;

        return $releases;

    }

}

