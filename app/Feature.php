<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model {

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'features';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'status', 'score', 'completion_date'];

    //ID WILL BE "REFERENCE NUMBER" FOR NOW

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];

    protected $hidden = ['password', 'remember_token'];

    public function release()
    {
        return $this->belongsTo('App\Release');
    }

    public function requirements () {

        return $this->hasMany('App\Requirement');

    }

}
